import { Dispatch, SetStateAction } from "react";

export interface IDropdownItem {
    children?: string;
    goToMenu?: string;
    leftIcon: JSX.Element;
    link?: string;
    setActiveMenu: Dispatch<SetStateAction<string>>;
}