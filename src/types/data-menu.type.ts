export interface IDataMenu {
    parentId?: string;
    id: string;
    name: string;
    link?: string;
    submenu?: IDataMenu[];
}