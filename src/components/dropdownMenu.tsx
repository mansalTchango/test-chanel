import { motion } from "framer-motion";
import React, { useState } from "react";
import useMeasure from "react-use-measure";
import dataJson from "../data/menu.data.json";
import { ReactComponent as ArrowLeft } from "../icons/arrow-left.svg";
import { ReactComponent as ArrowRight } from "../icons/arrow-right.svg";
import "../index.css";
import { IDataMenu } from "../types/data-menu.type";
import DropdownItem from "./dropdownItem";

function DropdownMenu() {
  const [activeMenu, setActiveMenu] = useState("main");
  const [ref, { height }] = useMeasure();
  const data = dataJson;

  function RecursiveMenu({ parentId, id, submenu }: IDataMenu) {
    const hasChildren = (menu: IDataMenu[]) => menu && menu.length;

    return (
      <>
        {activeMenu === id && (
          <div ref={ref} style={{ height: `${height || "auto"}` }} className="menu">
            {parentId && <DropdownItem goToMenu={parentId} leftIcon={<ArrowLeft />} setActiveMenu={setActiveMenu} />}
            {submenu &&
              submenu.map((item) => (
                <DropdownItem
                  key={item.id}
                  link={item.link}
                  leftIcon={<ArrowRight />}
                  goToMenu={item.submenu ? item.id : undefined}
                  setActiveMenu={setActiveMenu}
                >
                  {item.name}
                </DropdownItem>
              ))}
          </div>
        )}
        {submenu && hasChildren(submenu) && submenu.map((item) => <RecursiveMenu key={item.id} {...item} />)}
      </>
    );
  }

  return (
    <motion.div animate={{ height: height || "auto" }} transition={{ duration: 0.1 }} className="dropdown">
      <RecursiveMenu {...data} />
    </motion.div>
  );
}
export default DropdownMenu;
