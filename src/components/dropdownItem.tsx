import "../index.css";
import { IDropdownItem } from "../types/dropdown-item.type";

function DropdownItem(props: IDropdownItem) {
  return (
    <a
      href={props.link}
      className="menu-item"
      onClick={() => props.goToMenu && props.setActiveMenu(props.goToMenu)}
    >
      {props.children}
      {props.goToMenu && (
        <span className={`icon-button ${props.children ? "icon-right" : ""} `}>
          {props.leftIcon}
        </span>
      )}
    </a>
  );
}

export default DropdownItem;
